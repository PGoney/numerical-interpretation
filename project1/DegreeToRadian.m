function ret = DegreeToRadian(x)
    ret = x*pi/180;
end