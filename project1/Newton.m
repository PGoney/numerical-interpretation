function [history, theta] = Newton(myfunc, x, N, TOL, TOL_X)
    myfunc_prime = diff(myfunc);
    fx = double(subs(myfunc, x));
    fp = double(subs(myfunc_prime, x));
    history = [0, x, fx, fp, NaN, NaN];
    
    for i = 1:N
        fx = double(subs(myfunc, x));
        fp = double(subs(myfunc_prime, x));
        if abs(fp) < TOL_X
            disp('Problem: Initial value');
            theta = NaN;
            return
        end
        
        d = fx / fp;
        old_x = x;
        x = x - d;
        
        history = [history; [i, old_x, fx, fp, x, d]];
        
        if abs(d) < TOL
            theta = x;
            return
        end
    end
end