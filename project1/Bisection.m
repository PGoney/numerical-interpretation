function [history, theta] = Bisection(myfunc, x0, x1, N, TOL)
    f_x0 = double(subs(myfunc, x0));
    f_x1 = double(subs(myfunc, x1));
    history = [0, x0, f_x0, x1, f_x1, NaN, NaN];
    
    if sign(f_x0) == sign(f_x1)
        disp('Problem: Initial value')
        theta = NaN;
        return;
    end
    
    d = x1 - x0;
    for i = 1:N
        d = d/2;
        c = x0 + d;
        f_c = double(subs(myfunc, c));
        
        history = [history; [i, x0, f_x0, x1, f_x1, c, d]];
        
        if sign(f_x0) ~= sign(f_c)
            x1 = c;
            f_x1 = f_c;
        else
            x0 = c;
            f_x0 = f_c;
        end
        
        if abs(d) < TOL
            theta = c;
            return
        end
    end
    
    disp('Problem: error > max_err')
    theta = NaN;
end