function [history, theta] = Secant(myfunc, x0, x1, N, TOL, TOL_X)
    f_x0 = double(subs(myfunc, x0));
    history = [0, x0, f_x0, NaN, NaN, NaN];
    
    for i = 1:N
        f_x1 = double(subs(myfunc, x1));
        slope = (f_x1 - f_x0) / (x1 - x0);
        if slope < TOL_X
            disp('Problem: Initial value')
            theta = NaN;
        end
        x0 = x1;
        f_x0 = f_x1;
        d = f_x1/slope;
        x1 = x1 - f_x1/slope;
        history = [history; [i, x0, f_x0, slope, x1, d]];
        if (abs(d) < TOL)
            theta = x1;
            return
        end
    end
end
