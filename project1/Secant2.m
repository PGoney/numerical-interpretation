function [history, ret] = Secant2(myfunc, a, b, nmax, e)
    fa = double(subs(myfunc, a));
    fb = double(subs(myfunc, b));
    if abs(fa) > abs(fb)
        [b, a] = deal(a, b);
        [fb, fa] = deal(fa, fb);
    end
    history = [0, a, fa];
    history = [history; [1, b, fb]];
    for n = 2:nmax
        if abs(fa) > abs(fb)
            [b, a] = deal(a, b);
            [fb, fa] = deal(fa, fb);
        end
        d = (b - a) / (fb - fa);
        b = a;
        fb = fa;
        d = d*fa;
        if abs(d) < e
            ret = b;
            return
        end
        a = a - d;
        fa = double(subs(myfunc, a));
        history = [history; [n, a, fa]];
    end
end