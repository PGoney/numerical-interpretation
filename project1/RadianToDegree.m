function ret = RadianToDegree(x)
    ret = x*180/pi;
end