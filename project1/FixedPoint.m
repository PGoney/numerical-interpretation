function [history, theta] = FixedPoint(myfunc, myfunc_u, myfunc_g, x, N, tol)
    ux = double(subs(myfunc_u, x));
    history = [0, x, ux, NaN, NaN, NaN, NaN];
    theta = 0;
    
    for i=1:N
        ux = double(subs(myfunc_u, x));
        gx = double(subs(myfunc_g, x));
        gp = double(subs(diff(myfunc_g), x));
        d = x - gx;
        old_x = x;
        x = gx;
        fx = double(subs(myfunc, old_x));
        
        history = [history; [i, old_x, ux, x, fx, gp, d]];
        
        if abs(d) < tol
            theta = x;
            return
        end
    end
end