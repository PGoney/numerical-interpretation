clear; clc;

syms x y

r1 = 10; r2 = 6; r3 = 8; r4 = 4;
R1 = r1/r2;
R2 = r1/r4;
R3 = (r1^2 + r2^2 - r3^2 + r4^2)/(2*r2*r4);

N = 100;
TOL = DegreeToRadian(0.000001);

PARAMETER = [
    10, 5, 15;      % -2.3640, 8.0693
    40, 30, 40;     % -9.7471, 32.0152
    70, 50, 60;     % -18.2956, 54.8878
    100, 70, 80;	% -29.3824, 75.2709
    130, 85, 95;   % -45.2947, 90.1241
    160, 90, 100;   % -68.0075, 92.7350
    ];

% fp = fopen('Result.txt', 'w');

for i=1:6
    myfunc = R1*cos(x) - R2*cos(y) + R3 - cos(x - y);
    alpha = DegreeToRadian(PARAMETER(i, 1));
    myfunc = subs(myfunc, alpha);

    x0 = DegreeToRadian(PARAMETER(i, 2));
    x1 = DegreeToRadian(PARAMETER(i, 3));
    
%     disp(RadianToDegree(alpha))
%     disp(RadianToDegree(x0))
%     disp(RadianToDegree(x1))

%     [history, theta] = Bisection(myfunc, x0, x1, N, TOL);
%     history(:, 2) = RadianToDegree(history(:, 2));
%     history(:, 4) = RadianToDegree(history(:, 4));
%     history(:, 6) = RadianToDegree(history(:, 6));
%     history(:, 7) = RadianToDegree(history(:, 7));
%     theta = RadianToDegree(theta);
%     disp('Bisection')
%     fp = fopen('Bisection.txt', 'w');
% %     fprintf(fp, '%s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\n', 'n', 'a', 'f(a)', 'b', 'f(b)', 'c', 'err');
% %     fprintf(fp, '%d\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\n', history');
%     fprintf('%.07f\n', theta);
%     fclose(fp);
    
%     [history, theta] = Newton(myfunc, x0, N, TOL, 0.00001);
%     history(:, 2) = RadianToDegree(history(:, 2));
%     history(:, 4) = DegreeToRadian(history(:, 4));
%     history(:, 5) = RadianToDegree(history(:, 5));
%     history(:, 6) = RadianToDegree(history(:, 6));
%     theta = RadianToDegree(theta);
%     disp('Newton')
%     fp = fopen('Newton.txt', 'w');
% %     fprintf(fp, '%s\t%8s\t%8s\t%8s\t%8s\t%8s\n', 'n', 'a', 'f(a)', "f'(a)", 'c', 'err');
% %     fprintf(fp, '%d\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\n', history');
%     fprintf('%.07f\n', theta);
% %     fclose(fp);
% 
%     [history, theta] = Secant(myfunc, x0, x1, N, TOL, 0.000001);
%     history(:, 2) = RadianToDegree(history(:, 2));
%     history(:, 4) = DegreeToRadian(history(:, 4));
%     history(:, 5) = RadianToDegree(history(:, 5));
%     history(:, 6) = RadianToDegree(history(:, 6));
%     theta = RadianToDegree(theta);
%     disp('Secant')
% %     fp = fopen('Secant.txt', 'w');
% %     fprintf(fp, '%s\t%8s\t%8s\t%8s\t%8s\t%8s\n', 'n', 'a', 'f(a)', "g'(a)", 'c', 'err');
% %     fprintf(fp, '%d\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\n', history');
%     fprintf('%.07f\n', theta);
% %     fclose(fp);
% 
%     myfunc_u = 1/R2 * (R1*cos(x) + R3 - cos(x - y));
%     myfunc_u = subs(myfunc_u, alpha);
%     myfunc_g = acos(myfunc_u);
%     [history, theta] = FixedPoint(myfunc, myfunc_u, myfunc_g, x0, N, TOL);
%     history(:, 2) = RadianToDegree(history(:, 2));
%     history(:, 4) = RadianToDegree(history(:, 4));
%     % history(:, 5) = RadianToDegree(history(:, 5));
%     history(:, 7) = RadianToDegree(history(:, 7));
%     theta = RadianToDegree(theta);
%     disp('FixedPoint')
%     fp = fopen('FixedPoint.txt', 'w');
%     fprintf(fp, '%s\t%8s\t%8s\t%8s\t%8s\t%8s\t%8s\n', 'n', 'a', 'u(a)', 'c', 'f(a)', "g'(a)", 'err');
%     fprintf(fp, '%d\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\t%8.07f\n', history');
%     fprintf(fp, '%.07f\n', theta);
%     fclose(fp);

    myfunc_u = R1*cos(x) + R3 - R2*cos(y);
%     myfunc_u = 1/R2 * (R1*cos(x) + R3 - cos(x - y));
    myfunc_u = subs(myfunc_u, alpha);
    myfunc_g = -acos(myfunc_u) + alpha;
    [history, theta] = FixedPoint(myfunc, myfunc_u, myfunc_g, x0, N, TOL);
    history(:, 2) = RadianToDegree(history(:, 2));
    history(:, 4) = RadianToDegree(history(:, 4));
    theta = RadianToDegree(theta);
%     disp('FixedPoint')
%     fprintf('%d\t%10.07f\t%10.07f\t%10.07f\t%10.07f\t%10.07f\t%10.07f\n', history');
    size(history)
    fprintf('%.07f\n', theta);
end
% fclose(fp)