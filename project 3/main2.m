clear; clc;
n_list = [5];

for i=1:length(n_list)
    n = n_list(i);
    A = gallery("tridiag", n, -5, 1, 2);
    b = A*ones(n, 1);
    
    [D, L, U] = Decomposite(A, 'tridiag');
    
    output = [];
    
    x = GE(D, L, U, b, false);
    err1 = norm(b - A*x, 2);
    output = [output, x];
    
    x = GE(D, L, U, b, true);
    err2 = norm(b - A*x, 2);
    output = [output, x];
    
    disp("optimal x")
    fprintf("%s\t\t%s\n", "Base", "Pivoting");
    fprintf("%f\t%f\n", output');
    fprintf("\n");
    disp("error")
    fprintf("%s\t\t%s\n", "Base", "Pivoting");
    fprintf("%e\t%e\n", err1, err2);
end