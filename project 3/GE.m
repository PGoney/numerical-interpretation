function ret = GE(D, L, U, b, pivoting)
    n = length(D);
    x = zeros(n, 1);

    if pivoting == false
        for i=1:n-1
            pivot = D(i);
            multiplier = L(i)/pivot;
            
            L(i) = L(i) - multiplier*D(i);
            D(i+1) = D(i+1) - multiplier*U(i);
            b(i+1) = b(i+1) - multiplier*b(i);
        end
        
        x(n) = b(n)/D(n);
        for i=n-1:-1:1
            x(i) = (b(i) - U(i)*x(i+1))/D(i);
        end
    elseif pivoting == true
        U2 = zeros(n-2, 1);
        for i=1:n-2
            if abs(D(i)) < abs(L(i))
                temp_D = D(i); temp_U = U(i); temp_U2 = U2(i); temp_b = b(i);
                D(i) = L(i); U(i) = D(i+1); U2(i) = U(i+1); b(i) = b(i+1);
                L(i) = temp_D; D(i+1) = temp_U; U(i+1) = temp_U2; b(i+1) = temp_b;
            end
            
            pivot = D(i);
            multiplier = L(i)/pivot;
            
            L(i) = L(i) - multiplier*D(i);
            D(i+1) = D(i+1) - multiplier*U(i);
            U(i+1) = U(i+1) - multiplier*U2(i);
            b(i+1) = b(i+1) - multiplier*b(i);
        end
        if abs(D(n-1)) < abs(L(n-1))
            temp_D = D(n-1); temp_U = U(n-1); temp_b = b(n-1);
            D(n-1) = L(n-1); U(n-1) = D(n); b(n-1) = b(n);
            L(n-1) = temp_D; D(n) = temp_U; b(n) = temp_b;
        end
        
        pivot = D(n-1);
        multiplier = L(n-1)/pivot;
        
        L(n-1) = L(n-1) - multiplier*D(n-1);
        D(n) = D(n) - multiplier*U(n-1);
        b(n) = b(n) - multiplier*b(n-1);
        
        x(n) = b(n)/D(n);
        x(n-1) = (b(n-1) - U(n-1)*x(n))/D(n-1);
        for i=n-2:-1:1
            x(i) = (b(i) - U(i)*x(i+1) - U2(i)*x(i+2))/D(i);
        end
    end
    ret = x;
end

function show(D, L, U, U2, b)
    n = length(D);
    A = zeros(n, n+1);
    
    for i=1:n-2
        A(i,i) = D(i);
        A(i+1,i) = L(i);
        A(i,i+1) = U(i);
        A(i,i+2) = U2(i);
        A(i,n+1) = b(i);
    end
    A(n-1,n-1) = D(n-1);
    A(n,n-1) = L(n-1);
    A(n-1,n) = U(n-1);
    A(n-1,n+1) = b(n-1);
    A(n,n) = D(n);
    A(n,n+1) = b(n);
    disp(A)
end