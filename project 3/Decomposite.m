function [D, L, U] = Decomposite(A, structure)
    if structure == "tridiag"
        [m, n] = size(A);
        D = zeros(n, 1);
        L = zeros(n-1, 1);
        U = zeros(n-1, 1);
        
        D(1) = A(1, 1);
        for i = 2:n
            D(i) = A(i, i);
            L(i-1) = A(i, i-1);
            U(i-1) = A(i-1, i);
        end
    end
end