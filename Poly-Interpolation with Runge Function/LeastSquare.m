function ret = LeastSquare(A, b)
    x = GE(A'*A, A'*b);
    ret = x;
end