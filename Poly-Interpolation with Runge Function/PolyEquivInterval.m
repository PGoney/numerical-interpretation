function p = PolyEquivInterval(myfunc, a, b, n)
    x = linspace(a, b, n);
    y = myfunc(x);
    c = CoefByDividedDifferences(x, y);
end