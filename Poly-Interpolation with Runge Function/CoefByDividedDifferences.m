function ret = CoefByDividedDifferences(x, y)
    n = size(x);
    c = y;
    for j = 2:n
        for i = n:-1:j
            c(i) = (c(i) - c(i-1))/(x(i) - x(i-j+1));
        end
    end
    ret = c;
end