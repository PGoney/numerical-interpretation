function ret = GetError(y, p)
    ret = abs(y-p);
end