a = -5; b = 5;
n=[5, 7, 8, 10, 15, 20]';
% 
% for i = 1:6
%     x = linspace(a, b, n(i))';
%     y = myfunc(x);
%     c = CoefByDividedDifferences(x, y);
%     
%     x_span = linspace(a, b, 1000)';
%     y_exact = myfunc(x_span);
%     y_span = HornersRule(x, c, x_span);
%     
%     err = GetError(y_exact, y_span);
%     
%     figure
%     hold on
%     plot(x_span, y_exact)
%     plot(x_span, y_span)
%     hold off
%     figure
%     plot(x_span, err)
% end

% for i = 6:6
%     x = ChebyshevNodes(a, b, n(i));
%     y = myfunc(x);
%     c = CoefByDividedDifferences(x, y);
%     
%     x_span = linspace(a, b, 1000)';
%     y_exact = myfunc(x_span);
%     y_span = HornersRule(x, c, x_span);
%     
%     err = GetError(y_exact, y_span);
%     
%     figure
%     hold on
%     plot(x_span, y_exact)
%     plot(x_span, y_span)
%     hold off
%     figure
%     plot(x_span, err)
% end

x_span = [];
y_span = [];
partition = linspace(a, b, 6)';
for i = 1:5
    x = linspace(partition(i), partition(i+1), 4)';
    y = myfunc(x);
    A = [1;1;1;1];
    for j = 1:3
        A = [A, x .^ j];
    end
    c = LeastSquare(A, y);
    
    x_span_temp = linspace(partition(i), partition(i+1), 1000)';
    y_span_temp = c(1) + c(2).*x_span_temp + c(3).*(x_span_temp.^2) + c(4).*(x_span_temp.^3);
    x_span = [x_span; x_span_temp];
    y_span = [y_span; y_span_temp];
end

y_exact = myfunc(x_span);
hold on
plot(x_span, y_exact)
plot(x_span, y_span)

function ret = myfunc(x)
    ret = 1./(1+x.^2);
end