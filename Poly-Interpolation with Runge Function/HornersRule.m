function ret = HornersRule(x, c, t)
    n = length(x);
    value = zeros(length(t), 1);
    value = value + c(n);
    for i = n-1:-1:1
        value = value.*(t - x(i)) + c(i);
    end
    ret = value;
end